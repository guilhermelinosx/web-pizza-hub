# <div align="center"> Web Pizza Hub </div>

</br>

<div align="center">
<p>🚧 It is in Development 🚧</p>
</div>

</br>

## Technologies used in the project

- Typescript
- NextJs
- Axios


## How to run the project

- Clone this repository

```shell
git clone https://github.com/guilhermelinosx/web-pizza-hub.git
```

</br>

- Start the Application in Development

```shell
yarn install
yarn dev
```

</br>

- To stop the Application click CTRL+C in your terminal
