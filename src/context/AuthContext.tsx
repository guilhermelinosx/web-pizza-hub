import { createContext, ReactNode, useState, useEffect } from 'react'
import { destroyCookie, setCookie, parseCookies } from 'nookies'
import Router from 'next/router'
import { api } from '../services/api'
import { toast } from 'react-toastify'

type AuthContextData = {
  user: UserProps
  isAuthenticated: boolean
  signIn: (credentials: SignInProps) => Promise<void>
  signUp: (credentials: SignUpProps) => Promise<void>
  forgotPassword: (credentials: ForgotPasswordProps) => Promise<void>
  resetPassword: (credentials: ResetPasswordProps) => Promise<void>
  signOut: () => void
}

type UserProps = {
  id: string
  name: string
  email: string
}

type SignInProps = {
  email: string
  password: string
}

type SignUpProps = {
  name: string
  email: string
  password: string
}

type ForgotPasswordProps = {
  email: string
}

type ResetPasswordProps = {
  password: string
}

type AuthProviderProps = {
  children: ReactNode
}

export const AuthContext = createContext({} as AuthContextData)

export function signOut() {
  destroyCookie(undefined, '@pizzaHub:token')
  destroyCookie(undefined, '@pizzaHub:user')

  if (typeof window !== 'undefined') {
    Router.push('/')
  } else {
    alert('Error on sign out')
  }
}

export function AuthProvider({ children }: AuthProviderProps) {
  const [user, setUser] = useState<UserProps>({
    id: '',
    name: '',
    email: ''
  } as UserProps)

  const isAuthenticated = !!user

  const signIn = async ({ email, password }: SignInProps): Promise<void> => {
    try {
      const res = await api.post('/signin', {
        email,
        password
      })

      const { token, user } = res.data

      setCookie(undefined, '@pizzaHub:token', token, {
        maxAge: 60 * 60 * 24,
        path: '/'
      })

      setCookie(undefined, '@pizzaHub:user', JSON.stringify(user.id), {
        maxAge: 60 * 60 * 24,
        path: '/'
      })

      setUser({
        id: user.id,
        name: user.name,
        email: user.email
      })

      api.defaults.headers['Authorization'] = `Bearer ${token}`

      Router.push('/dashboard')
    } catch (err) {
      toast.error('Email or password incorrect')
      console.log(err)
    }
  }

  const signUp = async ({ name, email, password }: SignUpProps): Promise<void> => {
    try {
      await api.post('/signup', {
        name,
        email,
        password
      })

      toast.success('Account created successfully')

      Router.push('/')
    } catch (err) {
      toast.error('Email already in use')
      console.log(err)
    }
  }

  const forgotPassword = async ({ email }: ForgotPasswordProps): Promise<void> => {
    try {
      const res = await api.post('/forgotpassword', {
        email
      })

      console.log(res.data)

      setCookie(undefined, '@pizzaHub:tokenResetPassword', res.data, {
        maxAge: 60 * 60 * 2,
        path: '/resetpassword'
      })
    } catch (err) {
      toast.error('Email not found')
      console.log(err)
    }
  }

  const resetPassword = async ({ password }: ResetPasswordProps): Promise<void> => {
    const { '@pizzaHub:tokenResetPassword': tokenResetPassword } = parseCookies()

    try {
      await api.post('/resetpassword', {
        password,
        token: tokenResetPassword
      })

      toast.success('Passwords changed successfully')

      Router.push('/')

      destroyCookie(undefined, '@pizzaHub:tokenResetPassword')
    } catch (err) {
      console.log(err)
    }
  }

  return (
    <AuthContext.Provider
      value={{ user, isAuthenticated, signIn, signOut, signUp, forgotPassword, resetPassword }}
    >
      {children}
    </AuthContext.Provider>
  )
}
