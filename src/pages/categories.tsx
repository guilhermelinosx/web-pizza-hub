import React from 'react'
import Head from 'next/head'
import { Button } from '../components/Button'
import { Header } from '../components/Header'
import { Input } from '../components/Input'
import styles from '../styles/categories.module.scss'
import { SSRAuth } from '../utils/ssr/SSRAuth'
import { api } from '../services/api'
import { toast } from 'react-toastify'

export default function Categories() {
  const [name, setName] = React.useState('')

  React.useEffect(() => {
    api.get('/categories').then(response => console.log(response.data))
  }, [])

  const handleCreateCategory = async (event: React.FormEvent) => {
    event.preventDefault()

    if (name === '') {
      toast.warning('Name is required')

      return
    }

    try {
      await api.post('/categories', {
        name
      })

      toast.success('Category created successfully')

      setName('')
    } catch (err) {
      toast.error('Category already exists')
      setName('')
    }
  }

  const handleGetCategory = async (event: React.FormEvent) => {
    event.preventDefault()

    try {
      const res = await api.get('/categories')
      alert('Categories retrieved successfully')

      console.log(res.data)

      res.data.map((category: any) => {
        console.log(category.name)
      })
    } catch (err) {
      alert('Categories does not exist')
    }
  }

  const handleDeleteCategory = async (event: React.FormEvent) => {
    event.preventDefault()

    if (name === '') {
      alert('Name is required')
      return
    }

    try {
      const res = await api.delete(`/categories/${name}`)
      alert('Category deleted successfully')
    } catch (err) {
      alert('Category does not exist')
    }
  }

  const handleUpdateCategory = async (event: React.FormEvent) => {
    event.preventDefault()

    if (name === '') {
      alert('Name is required')
      return
    }

    try {
      const res = await api.put(`/categories/${name}`)
      alert('Category updated successfully')
    } catch (err) {
      alert('Category does not exist')
    }
  }

  return (
    <>
      <Head>
        <title>Categories | Pizza Hub</title>
      </Head>
      <Header />
      <main className={styles.container}>
        <h1>New Category</h1>

        <form onSubmit={handleCreateCategory} className={styles.content}>
          <Input
            placeholder="Enter a new Category"
            type="text"
            autoCapitalize="none"
            value={name}
            onChange={name => setName(name.target.value)}
          />

          <Button type="submit">Create</Button>
        </form>
      </main>
    </>
  )
}

export const getServerSideProps = SSRAuth(async ctx => {
  return {
    props: {}
  }
})
