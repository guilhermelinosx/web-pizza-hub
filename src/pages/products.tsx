import React from 'react'
import Head from 'next/head'
import { Button } from '../components/Button'
import { Header } from '../components/Header'
import { Input } from '../components/Input'
import { TextArea } from '../components/TextArea'
import styles from '../styles/products.module.scss'
import { SSRAuth } from '../utils/ssr/SSRAuth'
import { api, setupAPIClient } from '../services/api'
import { toast } from 'react-toastify'

type ItemProps = {
  id: string
  name: string
}

interface CategoryProps {
  categoryList: ItemProps[]
}

export default function Products({ categoryList }: CategoryProps) {
  const [name, setName] = React.useState('')
  const [description, setDescription] = React.useState('')
  const [price, setPrice] = React.useState('')
  const [categories, setCategories] = React.useState(categoryList || [])
  const [selectedCategory, setSelectedCategory] = React.useState(0)

  const handleCreateProduct = async (event: React.FormEvent) => {
    event.preventDefault()

    if (name === '' || description === '' || price === '') {
      toast.warning('All fields are required')
      return
    }

    try {
      await api.post('/products', {
        name,
        description,
        price,
        category_id: categories[selectedCategory].id
      })

      toast.success('Product created successfully')
    } catch (err) {
      toast.error('Error creating product')
    }

    setName('')
    setPrice('')
    setDescription('')
  }

  const handleCategoryChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedCategory(Number(event.target.value))
  }

  return (
    <>
      <Head>
        <title>Products | Pizza Hub</title>
      </Head>
      <Header />
      <main className={styles.container}>
        <h1>New Product</h1>

        <form className={styles.content} onSubmit={handleCreateProduct}>
          <select value={selectedCategory} onChange={handleCategoryChange}>
            {categories.map((category, index) => {
              return (
                <option key={category.id} value={index}>
                  {category.name}
                </option>
              )
            })}
          </select>

          <Input
            placeholder="Name"
            type="text"
            autoCapitalize="none"
            value={name}
            onChange={name => setName(name.target.value)}
          />
          <Input
            placeholder="Price"
            type="number"
            autoCapitalize="none"
            value={price}
            onChange={price => setPrice(price.target.value)}
          />

          <TextArea
            placeholder="Description"
            autoCapitalize="none"
            value={description}
            onChange={description => setDescription(description.target.value)}
          ></TextArea>

          <Button type="submit">Create</Button>
        </form>
      </main>
    </>
  )
}

export const getServerSideProps = SSRAuth(async ctx => {
  const api = setupAPIClient(ctx as any)

  const { data } = await api.get('/categories')

  return {
    props: {
      categoryList: data
    }
  }
})
