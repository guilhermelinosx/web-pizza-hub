import { useState } from 'react'
import Head from 'next/head'
import styles from '../styles/dashboard.module.scss'
import { FiRefreshCcw } from 'react-icons/fi'
import Modal from 'react-modal'
import { SSRAuth } from '../utils/ssr/SSRAuth'
import { ModalOrder } from '../components/ModalOrder'
import { setupAPIClient, api } from '../services/api'
import { Header } from '../components/Header'

type OrderProps = {
  id: string
  table: string | number
  status: boolean
  draft: boolean
  name: string | null
}

interface HomeProps {
  orders: OrderProps[]
}

export type OrderItemProps = {
  id: string
  amount: number
  order_id: string
  product_id: string
  product: {
    id: string
    name: string
    description: string
    price: string
    banner: string
  }
  order: {
    id: string
    table: string | number
    status: boolean
    name: string | null
  }
}

export default function Dashboard({ orders }: HomeProps) {
  const [orderList, setOrderList] = useState(orders || [])

  const [modalItem, setModalItem] = useState<OrderItemProps[]>()
  const [modalVisible, setModalVisible] = useState(false)

  function handleCloseModal() {
    setModalVisible(false)
  }

  async function handleOpenModalView(id: string) {
    const response = await api.get(`/orders/detail/${id}`)

    setModalItem(response.data)
    setModalVisible(true)
  }

  async function handleFinishItem(id: string) {
    await api.put(`/orders/finish/${id}`)

    const { data } = await api.get('/orders')

    setOrderList(data)
    setModalVisible(false)
  }

  async function handleRefreshOrders() {
    const { data } = await api.get('/orders')
    setOrderList(data)
  }

  Modal.setAppElement('#__next')

  return (
    <>
      <Head>
        <title>Painel - Sujeito Pizzaria</title>
      </Head>
      <div>
        <Header />

        <main className={styles.container}>
          <div className={styles.containerHeader}>
            <h1>Últimos pedidos</h1>
            <button onClick={handleRefreshOrders}>
              <FiRefreshCcw size={25} color="#3fffa3" />
            </button>
          </div>

          <article className={styles.ordersList}>
            {orderList.length === 0 && (
              <span className={styles.emptyList}>Nenhum pedido aberto foi encontrado...</span>
            )}

            {orderList.map(item => (
              <section key={item.id} className={styles.orderItem}>
                <button onClick={() => handleOpenModalView(item.id)}>
                  <div className={styles.tag}></div>
                  <span>Mesa {item.table}</span>
                </button>
              </section>
            ))}
          </article>
        </main>

        {modalVisible && (
          <ModalOrder
            isOpen={modalVisible}
            onRequestClose={handleCloseModal}
            order={modalItem}
            handleFinishOrder={handleFinishItem}
          />
        )}
      </div>
    </>
  )
}

export const getServerSideProps = SSRAuth(async ctx => {
  const api = setupAPIClient(ctx as any)

  const { data } = await api.get('/orders')

  return {
    props: {
      orders: data
    }
  }
})
