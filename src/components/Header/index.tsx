import React from 'react'
import Link from 'next/link'
import styles from './styles.module.scss'
import { FiLogOut } from 'react-icons/fi'
import { AuthContext } from '../../context/AuthContext'

export const Header = () => {
  const { signOut } = React.useContext(AuthContext)

  return (
    <header className={styles.header}>
      <div className={styles.content}>
        <div className={styles.content}>
          <Link href={'/dashboard'}>
            <h1>Pizza Hub</h1>
          </Link>
        </div>

        <div>
          <nav>
            <Link href={'/categories'}>
              <p>Categories</p>
            </Link>
            <Link href={'/products'}>
              <p>Products</p>
            </Link>

            <button onClick={signOut}>
              <FiLogOut color="#fff" size={24} />
            </button>
          </nav>
        </div>
      </div>
    </header>
  )
}
